import view.TSS as view
from init import Runner as initRun
from service import StockService

initRun()
print('params initialization is complete')

app = view.TraderSupportSystemApp(StockService.get_instance())
app.start()
