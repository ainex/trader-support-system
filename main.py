from init import Runner as initRun
from runtime import Params

from threading import Thread
from time import sleep
import time
import pandas as pd
import numpy as np

from in_out.reader import ReaderExcelSber

def threaded_function(arg):
    for i in range(arg):
        print("running in: ", arg)
        sleep(1)

if __name__ == '__main__':
    initRun()
    print('hi')

    params = Params.getInstance()
    # sber_df = params.data['sber_df']
    # print(sber_df.head())
    # print(sber_df.tail())
    # print(sber_df.size)

    # sber_price = sber_df.loc[:, 'price'].resample('5T').ohlc()
    # sber_vol = sber_df.loc[:, 'vol'].resample('5T').sum()

    reader_sber = ReaderExcelSber(directory=params.data['InitParams'].data['terminalExportDir'],
                    filename=params.data['InitParams'].data['sberDDEFile'])

    reader_sber.read()
    sber_df = reader_sber.data
    # reader_sber.keep_reading()

    print(sber_df.head())

    startTime = time.perf_counter()

    df_vwap = sber_df[:-5].groupby(pd.Grouper(freq='1Min')).apply(
        lambda mini_df: np.sum(mini_df.price * mini_df.vol) #/ np.sum(mini_df.vol)
    ).rename('vwap')
    print(df_vwap.tail())
    print('Elapsed time: {:6.3f} seconds for df_vwap '.format(time.perf_counter() - startTime))

    #create df gpouped and concat
    startTime = time.perf_counter()
    df_vwap_tail = sber_df[-5:].groupby(pd.Grouper(freq='1Min')).apply(
        lambda mini_df: np.sum(mini_df.price * mini_df.vol) #/ np.sum(mini_df.vol)
    ).rename('vwap')
    print(df_vwap_tail.tail())
    print('Elapsed time: {:6.3f} seconds for df_vwap_tail '.format(time.perf_counter() - startTime))