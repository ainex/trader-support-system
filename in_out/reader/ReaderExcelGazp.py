import pandas as pd

from in_out.reader.Reader import Reader as Parent
from threading import Thread
from time import sleep


class ReaderExcelSber(Parent):
    def __init__(self, directory, filename):
        super().__init__(directory, filename)
        self._dataframe = None
        self._names = ['number', 'ticker', 'date', 'time', 'price', 'vol', 'sum']
        self._usecols = 'A:G'
        self._parse_dates = {'timestamp': [2, 3]}

    def read(self):
        self._dataframe = pd.read_excel(self.filepath,
                                        index_col=0,
                                        names=self._names,
                                        usecols=self._usecols,
                                        parse_dates=self._parse_dates)

    def keep_reading(self):
        thread = Thread(target=self.update)
        thread.start()

    def update(self):
        while True:
            len_df = len(self._dataframe)
            print("len df: ", len_df)
            dataframe_update = pd.read_excel(self.filepath, index_col=0,
                                             names=self._names,
                                             usecols=self._usecols,
                                             parse_dates=self._parse_dates,
                                             skiprows=list(range(len_df)))
            sleep(10)
            print(dataframe_update.head(10))

    @property
    def data(self):
        return self._dataframe
