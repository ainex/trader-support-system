import pandas as pd

from in_out.reader.Reader import Reader as Parent


class ReaderJson(Parent):
    def __init__(self, directory, filename):
        super().__init__(directory, filename)
        self._data = None
        self._config = None

    def read(self):
        obj = pd.read_json(self.filepath, typ='series')
        self._data = obj['data']
        self._config = obj['config']

    @property
    def data(self):
        return self._data

    @property
    def config(self):
        return self._config
