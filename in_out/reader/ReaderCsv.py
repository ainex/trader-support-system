import pandas as pd

from in_out.reader.Reader import Reader as Parent


class ReaderCsv(Parent):
    def __init__(self, directory, filename):
        super().__init__(directory, filename)
        self._dataframe = None

    def read(self):
        self._dataframe = pd.read_csv(self.filepath)

    @property
    def data(self):
        return self._dataframe
