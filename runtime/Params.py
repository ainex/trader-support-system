class Params:
    '''
    The purpose of this object is to share Heavy-lifted objects. E.g. Pandas.DataFrame that holds huge table.
    '''
    # Single-tone design

    _instance = None

    @staticmethod
    def getInstance():
        """ Static access method. """
        if Params._instance is None:
            Params()

        return Params._instance

    def __init__(self):
        if Params._instance is not None:
            raise Exception("This class is a singleton!")
        else:
            Params._instance = self
        self._data = {}

    def add(self, key, value):
        self._data[key] = value

    @property
    def data(self):
        return self._data

    @property
    def dde_dir(self):
        return self._data['InitParams'].data['terminalExportDir']

    @property
    def sber_dde_file(self):
        return self._data['InitParams'].data['sberDDEFile']

    @property
    def gazp_dde_file(self):
        return self._data['InitParams'].data['gazpDDEFile']
