import numpy as np
import torch
from sklearn.preprocessing import StandardScaler

from commons import Constants as c
from model.Model import Model


class ModelService:
    """Transforms data to the Model scale and back"""
    _instance = None

    _models = {c.SBER: {c.T_1M: None, c.T_5M: None, c.T_15M: None}, c.GAZP: {c.T_1M: None, c.T_5M: None, c.T_15M: None}}
    _scalers = {c.SBER: StandardScaler(), c.GAZP: StandardScaler()}
    _windows = {c.SBER: {c.T_1M: 5, c.T_5M: 10, c.T_15M: 5}, c.GAZP: {c.T_1M: 50, c.T_5M: 20, c.T_15M: 7}}

    @staticmethod
    def get_instance():
        """ Static access method. """
        if ModelService._instance is None:
            ModelService()

        return ModelService._instance

    def get_window_size(self, ticker, timeframe):
        return self._windows[ticker][timeframe]

    def __init__(self):
        if ModelService._instance is not None:
            raise Exception('This class is a singleton')

        ModelService._instance = self

        self.load_scalers()
        self.load_models()

    def load_scalers(self):
        scaler_sber = StandardScaler()
        scaler_sber.mean_ = [242.9437985]
        scaler_sber.scale_ = [24.79266794]
        scaler_sber.var_ = [614.67638378]
        self._scalers[c.SBER] = scaler_sber

        scaler_gazp = StandardScaler()
        scaler_gazp.mean_ = [223.90082426]
        scaler_gazp.scale_ = [28.17071378]
        scaler_gazp.var_ = [793.58911475]
        self._scalers[c.GAZP] = scaler_gazp

    def load_models(self):
        model = Model(input_size=1, hidden_size=21, output_size=1)
        model.load_state_dict(torch.load('model\model2_sber_1M_5_1.pt'))
        model.eval()
        self._models[c.SBER][c.T_1M] = model

        model_sber_5M = Model(input_size=1, hidden_size=21, output_size=1)
        model_sber_5M.load_state_dict(torch.load('model\model2_sber_5M_10.pt'))
        model_sber_5M.eval()
        self._models[c.SBER][c.T_5M] = model_sber_5M

        model_sber_15M = Model(input_size=1, hidden_size=21, output_size=1)
        model_sber_15M.load_state_dict(torch.load('model\model1_sber_15M_5.pt'))
        model_sber_15M.eval()
        self._models[c.SBER][c.T_15M] = model_sber_15M

    def scale(self, ticker, df):
        scaled_arr = self._scalers[ticker].transform(df)
        return scaled_arr

    def unscale(self, ticker, ts):
        unscaled_val = self._scalers[ticker].inverse_transform(ts)
        return unscaled_val

    # result = generate_sequence(scaler, model_1, x_val[0:1])
    def generate_sequence(self, ticker, tf, x_sample, future=0):
        """ Generate future values for x_sample with the model """
        x_arr = self.scale(ticker, x_sample)
        x_arr = np.array(x_arr).reshape(-1, len(x_arr))
        x_val = torch.from_numpy(x_arr).float()
        model = self._models[ticker][tf]
        y_pred_tensor = model(x_val, future=future)
        y_pred = y_pred_tensor.cpu().tolist()
        y_pred = self.unscale(ticker, y_pred)
        return y_pred[0][-(future + 1):]
