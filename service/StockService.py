import time
from datetime import datetime

import matplotlib.dates as mpl_dates
import numpy as np
import pandas as pd

from commons import Constants as c
from runtime import Params
from service.ModelService import ModelService


class StockService:
    _instance = None
    _model_service = None
    _params = None
    _sber_df = None
    _mode = 'dev'
    _size = 20000
    _predictions = {c.SBER: {c.T_1M: {}, c.T_5M: {}, c.T_15M: {}}, c.GAZP: {c.T_1M: {}, c.T_5M: {}, c.T_15M: {}}}

    @staticmethod
    def get_instance():
        """ Static access method. """
        if StockService._instance is None:
            StockService()

        return StockService._instance

    def __init__(self):
        if StockService._instance is not None:
            raise Exception('This class is a singleton')

        StockService._instance = self
        self._params = Params.getInstance()
        self._mode = self._params.data['InitParams'].config['mode']
        self._sber_df = self._params.data['sber_df'][:self._size].copy()
        self._gazp_df = self._params.data['gazp_df'][:self._size].copy()
        self._model_service = ModelService.get_instance()

    def next(self):
        if self._mode == 'dev':
            self._size += 100
            t = time.process_time()
            self._sber_df = self._params.data['sber_df'][:self._size].copy()
            print('elapsed time2: ', time.process_time() - t)

    def get_sber_df(self):
        return self._sber_df

    def get_sber_last_price(self):
        return self._sber_df.loc[:, 'price'].iloc[-1]

    def get_gazp_last_price(self):
        return self._gazp_df.loc[:, 'price'].iloc[-1]

    def get_sber_price_ohlc(self, timeframe):
        """generates sequence of (time, open, high, low, close, ...)"""
        sber_price_ohlc = self._sber_df.loc[:, 'price'].resample(timeframe).ohlc()
        timelines = self.get_timeline(sber_price_ohlc.index[-1], timeframe)
        for i in timelines:
            new_row = pd.DataFrame(index=[i])
            sber_price_ohlc = sber_price_ohlc.append(new_row)
        sber_price_ohlc.insert(0, 'MPLDates', sber_price_ohlc.index.copy())
        sber_price_ohlc['MPLDates'] = sber_price_ohlc['MPLDates'].apply(mpl_dates.date2num)
        return sber_price_ohlc

    def get_vwap(self, ticker, timeframe, future=3):
        if ticker == c.SBER:
            return self.get_sber_vwap(timeframe, future)
        elif ticker == c.GAZP:
            return self.get_gazp_vwap(timeframe, future)
        else:
            return

    def get_sber_vwap(self, timeframe, future=3):
        sber_vwap = self._sber_df.groupby(pd.Grouper(freq=timeframe)).apply(
            lambda mini_df: np.sum(mini_df.price * mini_df.vol) / np.sum(mini_df.vol)
        ).to_frame(name='VWAP')
        predictions = self.get_prediction_future('SBER', timeframe, sber_vwap, future)
        predicted_periods = self.get_predicted_periods(sber_vwap.index[-1], timeframe, future + 1)
        for i, p in enumerate(predicted_periods):
            new_row = pd.DataFrame({'VWAP_P': predictions[i]}, index=[p])
            sber_vwap = sber_vwap.append(new_row)
        sber_vwap.insert(0, 'MPLDates', sber_vwap.index.copy())
        sber_vwap['MPLDates'] = sber_vwap['MPLDates'].apply(mpl_dates.date2num)
        signal = self.get_signal(predictions)
        return sber_vwap, signal

    def get_gazp_vwap(self, timeframe, future=3):
        gazp_vwap = self._gazp_df.groupby(pd.Grouper(freq=timeframe)).apply(
            lambda mini_df: np.sum(mini_df.price * mini_df.vol) / np.sum(mini_df.vol)
        ).to_frame(name='VWAP')
        predictions = self.get_prediction_future('GAZP', timeframe, gazp_vwap, future)
        predicted_periods = self.get_predicted_periods(gazp_vwap.index[-1], timeframe, future + 1)
        for i, p in enumerate(predicted_periods):
            new_row = pd.DataFrame({'VWAP_P': predictions[i]}, index=[p])
            gazp_vwap = gazp_vwap.append(new_row)
        gazp_vwap.insert(0, 'MPLDates', gazp_vwap.index.copy())
        gazp_vwap['MPLDates'] = gazp_vwap['MPLDates'].apply(mpl_dates.date2num)
        signal = self.get_signal(predictions)
        return gazp_vwap, signal

    def get_sber_vol(self, timeframe):
        sber_vol_sum = self._sber_df.loc[:, 'vol'].resample(timeframe).sum().to_frame()
        sber_vol_sum.insert(0, 'MPLDates', sber_vol_sum.index.copy())
        sber_vol_sum['MPLDates'] = sber_vol_sum['MPLDates'].apply(mpl_dates.date2num)
        return sber_vol_sum

    def get_gazp_vol(self, timeframe):
        gazp_vol_sum = self._gazp_df.loc[:, 'vol'].resample(timeframe).sum().to_frame()
        gazp_vol_sum.insert(0, 'MPLDates', gazp_vol_sum.index.copy())
        gazp_vol_sum['MPLDates'] = gazp_vol_sum['MPLDates'].apply(mpl_dates.date2num)
        return gazp_vol_sum

    def get_timeline(self, index, timeframe):
        today = datetime.date(index)
        timeline = pd.date_range(start=index, end=str(today) + ' 18:50:00', freq=timeframe)
        return timeline[1:]

    def get_predicted_periods(self, index, timeframe, future):
        futures = pd.date_range(start=index, periods=future + 1, freq=timeframe)
        return futures[1:]

    def get_prediction(self, ticker, timeframe, df_vwap, future):
        last_index = df_vwap.index[-1]
        print('ticker:{} timeframe:{} vwap last_index:{}'.format(ticker, timeframe, last_index))
        if last_index in self._predictions:
            return self._predictions[ticker][timeframe][last_index]
        window_size = self._model_service.get_window_size(ticker, timeframe)
        print('window size:{} future: {}'.format(window_size, future))

        future_vwaps = self._model_service.generate_sequence(ticker, timeframe, df_vwap[-window_size:], future)
        self._predictions[ticker][timeframe][last_index] = future_vwaps

        return future_vwaps

    def get_prediction_future(self, ticker, timeframe, df_vwap, future):
        window_size = self._model_service.get_window_size(ticker, timeframe)

        df_vwap_window = df_vwap[-window_size:].copy()
        last_index = df_vwap.index[-1]
        print('ticker:{} timeframe:{} vwap last_index:{}'.format(ticker, timeframe, last_index))
        if last_index in self._predictions:
            return self._predictions[ticker][timeframe][last_index]
        print('window size:{} future: {}'.format(window_size, future))

        future_vwaps = []
        predicted_periods = self.get_predicted_periods(last_index, timeframe, future + 1)
        for i, p in enumerate(predicted_periods):
            vwap_p = self._model_service.generate_sequence(ticker, timeframe, df_vwap_window[-window_size:], future=0)
            future_vwaps.append(vwap_p)
            new_row = pd.DataFrame({'VWAP': vwap_p}, index=[p])
            df_vwap_window = df_vwap_window.append(new_row)

        self._predictions[ticker][timeframe][last_index] = future_vwaps

        return future_vwaps

    def get_signal(self, predictions):
        direction = 0
        new_direction = 0
        changed_direction = False
        prev_value = predictions[0]
        i = 0
        while i < len(predictions) + 1:
            if (predictions[i + 1] - predictions[i]) > 0:
                new_direction = 1
            else:
                new_direction = -1
            if direction == 0:
                direction = new_direction
            if new_direction != direction:
                changed_direction = True

        if changed_direction:
            return 'HOLD'
        elif direction == 1:
            return 'BUY'
        else:
            return 'SELL'
