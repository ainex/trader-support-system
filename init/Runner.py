import os

from in_out import ReaderCsv
from in_out import ReaderExcelSber
from in_out import ReaderJson
from runtime import Params


class Runner:
    def __init__(self):
        params = Params.getInstance()
        init_params = ReaderJson(directory=os.getcwd(), filename='init.json')
        init_params.read()
        params.add('InitParams', init_params)

        reader_csv = ReaderCsv(directory=params.data['InitParams'].data['input'], filename='SBER_180101_200328.csv')
        reader_csv.read()
        params.add('dataframe', reader_csv.data)

        reader_sber = ReaderExcelSber(directory=params.data['InitParams'].data['terminalExportDir'],
                                      filename=params.data['InitParams'].data['sberDDEFile'])
        reader_sber.read()
        sber_df = reader_sber.data
        params.add('sber_df', sber_df)
        print('Loaded sber dataframe: ')
        print(sber_df.head())
