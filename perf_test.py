from init import Runner as initRun
from runtime import Params
import time
from in_out import ReaderExcel
import pandas as pd

initRun()

params = Params.getInstance()

t = time.process_time()
reader_sber = ReaderExcel(directory=params.data['InitParams'].data['terminalExportDir'],
                          filename=params.data['InitParams'].data['sberDDEFile'])
reader_sber.read()
elapsed_time = time.process_time() - t
print('elapsed time1: ', elapsed_time)

t = time.process_time()
sber_df = reader_sber.data
sber_df.columns = ['number', 'ticker', 'date', 'time', 'price', 'vol', 'sum']
elapsed_time = time.process_time() - t
print('elapsed time2: ', elapsed_time)


t = time.process_time()
sber_df['timestamp'] = pd.to_datetime(sber_df.date + 'T' + sber_df.time)

elapsed_time = time.process_time() - t
print('elapsed time3: ', elapsed_time)

sber_df.set_index('timestamp', inplace=True)
params.add('sber_df', sber_df)
print('Loaded sber dataframe: ')
print(sber_df.head())

reader_sber.readNext()
sber_df_next = reader_sber.update
reader_sber.readNext()
reader_sber.readNext()
