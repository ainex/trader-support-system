import os
import pathlib
import tkinter as tk
from tkinter import filedialog as fd
from tkinter import messagebox as mb
from tkinter import ttk
from tkinter.font import Font

import matplotlib.animation as animation
import matplotlib.dates as mdates
import matplotlib.ticker as mticker
from matplotlib import pyplot as plt
from matplotlib import style
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
from mpl_finance import candlestick_ohlc

from runtime import Params

LIGHT_COLOR = '#00A3E0'
DARK_COLOR = '#183A54'
FACECOLOR = '#EEEEEE'  # axes background color

LARGE_FONT = ('Verdana', 12)
NORMAL_FONT = ('Verdana', 10)
SMALL_FONT = ('Verdana', 8)

style.use('ggplot')

figure = plt.figure()

TICKER_SBER = 'SBER'
TICKER_GAZP = 'GAZP'
TICKER = TICKER_SBER
DATA_COUNTER = 9000
PROGRAM_NAME = 'sber'
VWAP = 'VWAP'
RESAMPLE_SIZE = '5Min'
CANDLE_WIDTH = 0.002
DATA_PACE = '1D'
INDICATOR_NONE = 'none'
INDICATOR = INDICATOR_NONE
VWAPs = []
CHART_LOAD = True
REFRESH_RATE = 1
PANE_COUNT = 1


def load_chart(run):
    global CHART_LOAD
    if run == 'start':
        CHART_LOAD = True
    elif run == 'stop':
        CHART_LOAD = False


def add_indicator(new_indicator):
    global INDICATOR
    global DATA_COUNTER

    if DATA_PACE == 'tick':
        alert('Indicators in tick are not available')

    elif new_indicator == INDICATOR_NONE:
        INDICATOR = new_indicator
        DATA_COUNTER = 9000
        print('set indicator to', INDICATOR)

    elif new_indicator == VWAP:
        vwap = tk.Tk()
        vwap.wm_title('Количество периодов')
        label = ttk.Label(vwap, text='Количество будущих периодов')
        label.pack(side=tk.TOP, fill='x', padx=10, pady=10)

        entry = ttk.Entry(vwap)
        entry.insert(0, 3)
        entry.pack(padx=10, anchor='w')
        entry.focus_set()

        def callback():
            global INDICATOR
            global DATA_COUNTER

            periods = (entry.get())
            group = [VWAP, periods]

            INDICATOR = group
            DATA_COUNTER = 9000
            print('set indicator to', group)
            vwap.destroy()

        button = ttk.Button(vwap, text='Вывести индикатор', width=20, command=callback)
        button.pack(padx=10, pady=5, anchor='w')
        tk.mainloop()


def change_time_frame(tf):
    global DATA_PACE
    global DATA_COUNTER
    if tf == '7D' and RESAMPLE_SIZE == '1min':
        alert('Too much data chosen, choose a smaller time frame')
    else:
        DATA_PACE = tf
        DATA_COUNTER = 9000


def change_sample_size(size, width):
    global RESAMPLE_SIZE
    global DATA_COUNTER
    global CANDLE_WIDTH
    if DATA_PACE == '7D' and RESAMPLE_SIZE == '1min':
        alert('Too much data chosen, choose a smaller time frame')
    elif DATA_PACE == 'tick':
        alert('no')
    else:
        RESAMPLE_SIZE = size
        DATA_COUNTER = 9000
        CANDLE_WIDTH = width


def change_stock(aim_stock, pn):
    global TICKER
    global DATA_COUNTER
    TICKER = aim_stock
    DATA_COUNTER = 9000


def alert(message):
    popup = tk.Tk()
    popup.wm_title('!')
    label = ttk.Label(popup, text=message, font=NORMAL_FONT)
    label.pack(side='top', fill='x', pady=10)
    button = ttk.Button(popup, text='Ok', command=popup.destroy)
    button.pack()
    popup.mainloop()


def animate(i, service):
    global DATA_COUNTER
    global REFRESH_RATE

    if CHART_LOAD:
        if PANE_COUNT == 1:
            try:
                service.next()
                main_plot = plt.subplot2grid((6, 4), (0, 0), rowspan=5, colspan=4)
                volume_plot = plt.subplot2grid((6, 4), (5, 0), rowspan=1, colspan=4, sharex=main_plot)

                params = Params.getInstance()
                # sber_df = service.get_sber_df()
                sber_price_ohlc = service.get_sber_price_ohlc(RESAMPLE_SIZE)
                sber_vol_sum = service.get_sber_vol(RESAMPLE_SIZE)

                main_plot.clear()

                # add VWAP indicator
                signal = 'HOLD'
                if INDICATOR is not INDICATOR_NONE:
                    if INDICATOR[0] == VWAP:
                        share_vwap, signal = service.get_vwap(RESAMPLE_SIZE, TICKER, int(INDICATOR[1]))
                        main_plot.plot(share_vwap['MPLDates'], share_vwap[VWAP], color='xkcd:dark pink', label=VWAP)
                        main_plot.plot(share_vwap['MPLDates'], share_vwap['VWAP_P'], ls='--', marker='*',
                                       color='xkcd:grey blue', label='VWAP_P')
                        main_plot.legend(loc=0)

                # add OHLC
                csticks = candlestick_ohlc(main_plot, sber_price_ohlc.values, width=CANDLE_WIDTH,
                                           colorup=LIGHT_COLOR, colordown=DARK_COLOR)
                main_plot.set_ylabel('Price')
                if RESAMPLE_SIZE == '1Min':
                    main_plot.set_xlim(None, sber_price_ohlc['MPLDates'][-350])
                else:
                    main_plot.set_xlim(None, sber_price_ohlc['MPLDates'][-1])

                main_plot.set_ylim(0.97 * sber_price_ohlc['low'][0], 1.01 * sber_price_ohlc['low'][0])
                main_plot.xaxis.set_major_locator(mticker.MaxNLocator(10))
                main_plot.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))

                volume_plot.fill_between(sber_vol_sum['MPLDates'], 0, sber_vol_sum['vol'], facecolor=DARK_COLOR)
                if RESAMPLE_SIZE == '1Min':
                    volume_plot.set_xlim(None, sber_price_ohlc['MPLDates'][-350])
                else:
                    volume_plot.set_xlim(None, sber_price_ohlc['MPLDates'][-1])

                volume_plot.set_ylabel('Volume')

                # hiding x legend
                plt.setp(main_plot.get_xticklabels(), visible=False)

                title = '{} -  {}  - Last price: {}  - {}'.format(TICKER, RESAMPLE_SIZE,
                                                                  str(service.get_sber_last_price()), signal)
                main_plot.set_title(title)
            except Exception as e:
                print('Failed because of:', e)

        else:
            if DATA_COUNTER > 12:
                try:
                    pass

                except Exception as e:
                    print('Failed in the non-tick animate ', str(e))


class TraderSupportSystemApp(tk.Tk):
    _service = None

    def __init__(self, service, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self._service = service
        self.geometry('1280x720')  # HD/WXGA

        tk.Tk.iconbitmap(self, default='resources/monitor_red.ico')
        tk.Tk.wm_title(self, '[Nova] - Система поддержки принятия решений для трейдера')

        container = tk.Frame(self)
        container.pack(side='top', fill='both', expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        menubar = tk.Menu(container)
        file_menu = tk.Menu(menubar, tearoff=0)
        file_menu.add_command(label='Сохранить настройки',
                              command=lambda: alert('Not implemented'))
        file_menu.add_separator()
        file_menu.add_command(label='Выход', command=self.quit())
        menubar.add_cascade(label='Файл', menu=file_menu)

        stock_choice = tk.Menu(menubar, tearoff=1)
        stock_choice.add_command(label='SBER', command=lambda: change_stock('Sberbank', TICKER_SBER))
        stock_choice.add_command(label='GAZP', command=lambda: change_stock('Gazprom', TICKER_GAZP))
        menubar.add_cascade(label='Акции', menu=stock_choice)

        data_time_frame = tk.Menu(menubar, tearoff=1)
        data_time_frame.add_command(label='1D', command=lambda: change_time_frame('1D'))
        data_time_frame.add_command(label='7D', command=lambda: change_time_frame('7D'))
        menubar.add_cascade(label='Масштаб', menu=data_time_frame)

        # time frame menu
        OHLCI = tk.Menu(menubar, tearoff=1)
        OHLCI.add_command(label='1min', command=lambda: change_sample_size('1Min', 0.0005))
        OHLCI.add_command(label='5min', command=lambda: change_sample_size('5Min', 0.002))
        OHLCI.add_command(label='15min', command=lambda: change_sample_size('15Min', 0.008))
        menubar.add_cascade(label='Таймфрейм', menu=OHLCI)

        # indicators menu
        indicator_menu = tk.Menu(menubar, tearoff=1)
        indicator_menu.add_command(label='Очистить', command=lambda: add_indicator(INDICATOR_NONE))
        indicator_menu.add_command(label=VWAP, command=lambda: add_indicator(VWAP))
        menubar.add_cascade(label='Индикатор', menu=indicator_menu)

        start_stop_menu = tk.Menu(menubar, tearoff=1)
        start_stop_menu.add_command(label='Возобновить прогнозирование', command=lambda: load_chart('start'))
        start_stop_menu.add_command(label='Пауза', command=lambda: load_chart('stop'))
        menubar.add_cascade(label='Прогнозировать/Пауза', menu=start_stop_menu)

        tk.Tk.config(self, menu=menubar)

        self.frames = {}
        for F in (StartPage, SberChartPage):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

    def start(self):
        figure.patch.set_facecolor(FACECOLOR)
        figure_animation = animation.FuncAnimation(figure, animate, fargs=(self._service,), interval=2000)
        self.mainloop()


class StartPage(tk.Frame):
    _controller = None
    _tree = None

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self._controller = controller
        label = tk.Label(self, text='NOVA - Система получения сигналов на основе прогноза индекса VWAP',
                         font=LARGE_FONT)
        label.pack(pady=10, padx=10)

        self.create_widgets()

    def create_widgets(self):
        dde_panel = tk.LabelFrame(self, text="Настройки DDE экспорта:", padx=10, pady=10)
        # dde_panel.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.Y)
        dde_panel.pack()
        self.create_treeview(dde_panel)
        self.load_data()

        button_frame = ttk.Frame(dde_panel)
        button_frame.pack()
        ttk.Button(button_frame, text="Изменить файл", command=self.set_file_name) \
            .pack(side=tk.LEFT, padx=5, pady=10)
        ttk.Button(button_frame, text='Начать сессию', command=lambda: self._controller.show_frame(SberChartPage)) \
            .pack(side=tk.RIGHT, padx=5, pady=10)

    def create_treeview(self, parent):
        f = ttk.Frame(parent)
        f.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.Y)
        f.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.Y)

        # create the tree
        self.data_cols = ('тикер', 'директория', 'файл')
        self._tree = ttk.Treeview(f, columns=self.data_cols, show='headings')

        # add tree to frame
        self._tree.grid(in_=f, row=0, column=0, sticky=tk.NSEW)

        # set frame resize priorities
        f.rowconfigure(0, weight=1)
        f.columnconfigure(0, weight=1)

    def load_data(self):
        params = Params.getInstance()
        self.data = [
            ('SBER', params.dde_dir, params.sber_dde_file),
            ('GAZP', params.dde_dir, params.gazp_dde_file)]

        # configure column headings
        for c in self.data_cols:
            self._tree.heading(c, text=c.title())
            self._tree.column(c, width=Font().measure(c.title()))

        # add data to the tree
        for item in self.data:
            self._tree.insert('', 'end', values=item)

            # and adjust column widths if necessary
            for idx, val in enumerate(item):
                i_width = Font().measure(val)
                if self._tree.column(self.data_cols[idx], 'width') < i_width:
                    self._tree.column(self.data_cols[idx], width=i_width)

    def set_file_name(self):
        file_name = fd.askopenfilename(filetypes=[("Excel files", ".xlsx .xls")])
        path = pathlib.Path(file_name)
        if path.exists() and path.is_file():
            print(os.path.dirname(file_name))
            print(os.path.basename(file_name))
            self.select_tree_item(os.path.dirname(file_name), os.path.basename(file_name))
        else:
            mb.showinfo("Сообщение", "Файл не был выбран")

    def select_tree_item(self, dir, file):
        selected_item = self._tree.focus()
        print(self._tree.item(selected_item))
        current_ticker = self._tree.item(selected_item, 'value')[0]
        self._tree.item(selected_item, values=(current_ticker, dir, file))


class SberChartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        buttons_frame = tk.Frame(self, pady=5)

        button_1M = ttk.Button(buttons_frame, text='1Min', command=lambda: change_sample_size('1Min', 0.0005))
        button_1M.pack(side=tk.LEFT)

        button_5M = ttk.Button(buttons_frame, text='5Min', command=lambda: change_sample_size('5Min', 0.002))
        button_5M.pack(side=tk.LEFT)

        button_15M = ttk.Button(buttons_frame, text='15Min', command=lambda: change_sample_size('15Min', 0.008))
        button_15M.pack(side=tk.LEFT)

        buttons_frame.pack()

        canvas = FigureCanvasTkAgg(figure, self)
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        toolbar = NavigationToolbar2Tk(canvas, self)
        toolbar.update()
        canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)
